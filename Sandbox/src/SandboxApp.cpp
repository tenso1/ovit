#include <Ovit.h>

class Sandbox : public Ovit::Application {
  public:
    Sandbox(){};
    ~Sandbox(){};
};

Ovit::Application* Ovit::CreateApplication() {
  return new Sandbox();
}