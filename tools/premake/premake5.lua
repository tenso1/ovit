 workspace "Ovit"
    architecture "x64"

    configurations{
        "Debug",
        "Release",
        "Dist"
    }

outputdir = "%{cfg.buildcgf}-%{cfg.system}-%{cfg.architecture}"

project "Ovit"
    location "Ovit"
    kind "SharedLib"
    language "C++"

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-intermediate/" .. outputdir .. "/%{prj.name}")

    files{
        "%{prj.name}/src/**.h",
        "%{prj.name}/src/**.cpp",
        "%{prj.name}/src/**.cc",
        "%{prj.name}/src/**.c"
    }

    includedirs{
        "%{prj.name}/external/spdlog/include"
    }

    filter "system:windows"
        cppdialect "C++17"
        staticruntime "On"
        systemversion "latest"

        defines{
            "OV_PLATFORM_WINDOWS",
            "OV_BUILD_DLL",
        }

        postbuildcommands{
            ("{COPY}%{cfg.buildtarget.relpath} ../bin/" .. outputdir .. "/Sandbox")
        }
    
        filter "configurations:Debug"
            defines "OV_DEBUG"
            symbols "On"

        filter "configurations:Debug"
            defines "OV_RELEASE"
            optimize "On"

        filter "configurations:Debug"
            defines "OV_DIST"
            optimize "On"

project "Sandbox"
    location "Ovit"
    kind "ConsoleApp"
    language "C++"

    targetdir ("bin/" .. outputdir .. "/%{prj.name}")
    objdir ("bin-intermediate/" .. outputdir .. "/%{prj.name}")
    
    files{
        "%{prj.name}/src/**.h",
        "%{prj.name}/src/**.cpp",
        "%{prj.name}/src/**.cc",
        "%{prj.name}/src/**.c"
    }

    includedirs{
        "Ovit/external/spdlog/include",
        "Ovit/src"
    }

    links{
        "Ovit"
    }

    filter "system:windows"
        cppdialect "C++17"
        staticruntime "On"
        systemversion "latest"

        defines{
            "OV_PLATFORM_WINDOWS",
        }
    
        filter "configurations:Debug"
            defines "OV_DEBUG"
            symbols "On"

        filter "configurations:Debug"
            defines "OV_RELEASE"
            optimize "On"

        filter "configurations:Debug"
            defines "OV_DIST"
            optimize "On"