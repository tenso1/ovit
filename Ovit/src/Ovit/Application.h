#pragma once

#include "Core.h"

namespace Ovit {

  class OVIT_API Application {
    public:
      Application();
      ~Application();

      void Run();
      
    };

  //Needs to be created on client
  Application* CreateApplication();
}
