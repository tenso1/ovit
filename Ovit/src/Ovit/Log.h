#pragma once

#include <memory>
#include "Core.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/stdout_color_sinks.h"

namespace Ovit {
  class OVIT_API Log {
  public:
    static void Init();

    inline static std::shared_ptr<spdlog::logger>& GetEngineLogger() { return s_EngineLogger; }
    inline static std::shared_ptr<spdlog::logger>& GetAppLogger() { return s_AppLogger; }

  private:
    static std::shared_ptr<spdlog::logger> s_EngineLogger;
    static std::shared_ptr<spdlog::logger> s_AppLogger;
  };
}

#define OV_ENGINE_TRACE(...) ::Ovit::Log::GetEngineLogger()->trace(__VA_ARGS__)
#define OV_ENGINE_INFO(...) ::Ovit::Log::GetEngineLogger()->info(__VA_ARGS__)
#define OV_ENGINE_WARNING(...) ::Ovit::Log::GetEngineLogger()->warn(__VA_ARGS__)
#define OV_ENGINE_ERROR(...) ::Ovit::Log::GetEngineLogger()->error(__VA_ARGS__)
#define OV_ENGINE_FATAL(...) ::Ovit::Log::GetEngineLogger()->fatal(__VA_ARGS__)

#define OV_APP_TRACE(...) ::Ovit::Log::GetAppLogger()->trace(__VA_ARGS__)
#define OV_APP_INFO(...) ::Ovit::Log::GetAppLogger()->info(__VA_ARGS__)
#define OV_APP_WARNING(...) ::Ovit::Log::GetAppLogger()->warn(__VA_ARGS__)
#define OV_APP_ERROR(...) ::Ovit::Log::GetAppLogger()->error(__VA_ARGS__)
#define OV_APP_FATAL(...) ::Ovit::Log::GetAppLogger()->fatal(__VA_ARGS__)

