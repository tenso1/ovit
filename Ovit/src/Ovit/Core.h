#pragma once

#ifdef OV_PLATFORM_WINDOWS
  #ifdef OV_BUILD_DLL
    #define OVIT_API __declspec(dllexport)
  #else
    #define OVIT_API __declspec(dllimport)
  #endif
#else
  #error Only windows supported! 
#endif