#include "Log.h"

namespace Ovit {

  std::shared_ptr<spdlog::logger> Log::s_EngineLogger;
  std::shared_ptr<spdlog::logger> Log::s_AppLogger;

  void Log::Init() {
    spdlog::set_pattern("%^[%T] %n: %v%$");

    s_EngineLogger = spdlog::stdout_color_mt("OVIT");
    s_EngineLogger->set_level(spdlog::level::trace);
    s_AppLogger = spdlog::stdout_color_mt("APP ");
    s_AppLogger->set_level(spdlog::level::trace);
  }
}
