#pragma once

#ifdef OV_PLATFORM_WINDOWS
extern Ovit::Application* Ovit::CreateApplication();

int main(int argc, char** argV) {
  printf("Welcome to Ovit Engine!\n");
  Ovit::Log::Init();
  OV_ENGINE_INFO("ENGINE Logger Initialized");
  OV_APP_INFO("App Logger Initialized {0}",90);
  auto app = Ovit::CreateApplication();
  app->Run();
  delete(app);
}

#else
#error Only windows supported! 
#endif